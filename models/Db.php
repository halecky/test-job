<?php

/**
 * Класс для работы с БД
 */

namespace models;

use PDO;


class Db
{
    const STATUS_ACTIVE = 0;
    const STATUS_FINISHED = 1;

    const ORDER_BY_NAME = 1;
    const ORDER_BY_EMAIL = 2;
    const ORDER_BY_STATUS = 3;
    /** @var PDO  */
    private $conn = null;


    const PATH_FILE_DATA = __DIR__ . "/../var/tasks.txt";
    const COUNT_ROWS_TO_FETCH = 3;

    public function __construct()
    {
        $this->connect();
    }

    public function selectTask($taskID)
    {
        /** @var \PDOStatement $sth */
        $sth = $this->conn->query("SELECT * FROM tasks WHERE id=$taskID");
        $data = $sth->fetch();
        return $data;
    }

    public function selectCountTasks()
    {
        $sth = $this->conn->query("SELECT COUNT(*) countRows FROM tasks");
        $data = $sth->fetch();
        return $data['countRows'];
    }

    public function select($orderBy = self::ORDER_BY_NAME, $orderAsc = true, $pageNum = 0)
    {

        $arrQuery[] = "SELECT * FROM tasks";
        switch ($orderBy)
        {
            case self::ORDER_BY_EMAIL: $fieldOrder = 'email'; break;
            case self::ORDER_BY_STATUS: $fieldOrder = 'status'; break;
            default: $fieldOrder = 'user_name'; break;
        }

        $arrQuery[] = " ORDER BY $fieldOrder";
        $arrQuery[] = $orderAsc ? "ASC": "DESC";

        $offset = $pageNum * self::COUNT_ROWS_TO_FETCH;
        $arrQuery[] = "LIMIT " . self::COUNT_ROWS_TO_FETCH . " OFFSET $offset";

        $query = implode(" ", $arrQuery);

        //var_dump($query);

        /** @var \PDOStatement $sth */
        $sth = $this->conn->query($query);
        $data = $sth->fetchAll();

        return $data;
    }

    public function insertTask($name, $email, $taskText)
    {

        $arrQuery[] = "INSERT tasks(user_name, email, text, status)";
        $arrQuery[] = "VALUES(:user_name, :email, :text, :status)";
        $query = implode(" ", $arrQuery);
        /** @var \PDOStatement $sth */
        $sth = $this->conn->prepare($query);

        $data['user_name'] = $name;
        $data['email'] = $email;
        $data['text'] = $taskText;
        $data['status'] = self::STATUS_ACTIVE;
//        $sth->bindValue("user_id", $userID);
//        $sth->bindValue("text", $taskText);
//        $sth->bindValue("status", self::STATUS_FINISHED);

        if ($sth->execute($data))
            return $this->conn->lastInsertId();

        return false;

    }

    /**
     * @param $taskID
     * @param array $data   example $data = ['status' => 1, 'text' => "some text"]
     * @return bool|string
     */
    public function updateTask($taskID, $data = [])
    {
        if (empty($data) || !is_array($data))
            return false;

        $arrValidFields = ['status', 'text'];
        foreach ($data as $field => $value) {
            if (in_array($field, $arrValidFields))
                $arrSet[] = "$field=:$field";
        }

        if (!isset($arrSet))
            return false;

        $query = "UPDATE tasks SET " . implode(",", $arrSet) . " WHERE id = $taskID";
        /** @var \PDOStatement $sth */
        $sth = $this->conn->prepare($query);

        if ($sth->execute($data))
            return $sth->rowCount();

        return false;
    }

    public function connect()
    {
        global $CONFIG;
        $host = $CONFIG['db']['host'];
        $dbname = $CONFIG['db']['dbname'];
        $login = $CONFIG['db']['user'];
        $password = $CONFIG['db']['password'];

        $dsn = "mysql:host=$host;dbname=$dbname;charset=utf8";

        $options = [
            PDO::MYSQL_ATTR_FOUND_ROWS   => TRUE,
            PDO::ATTR_PERSISTENT => true
            , PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            , PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ];

        try {
            $this->conn = new PDO($dsn, $login, $password, $options);
        } catch (PDOException $e) {
            die("DB Error<br>" . $e->getMessage());
        }

        return true;
    }



}