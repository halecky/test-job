<?php


namespace models;


class Task
{
    protected $_id = 0;

    public $text = "";
    public $status = 0;
    public $userName = "";
    public $userEmail = "";





    /**
     * Возвращает объект, инициализированный строкой данных из БД
     * @param $data
     * @return self
     */
    public static function &createTaskByData(& $data)
    {
        $obj = new self();
        $obj->status = $data['status'];
        $obj->text = $data['text'];
        $obj->userName = $data['user_name'];
        $obj->userEmail = $data['email'];
        $obj->_id = $data['id'];
        return $obj;
    }

    public static function createTaskById($taskID)
    {
        $db = new Db();
        $data = $db->selectTask($taskID);
        if (!is_array($data) || empty($data))
            return null;
        $task = self::createTaskByData($data);
        return $task;
    }

    public static function getCount()
    {
        $db = new Db();
        return $db->selectCountTasks();
    }


    /**
     * @param int $orderBy
     * @param bool $orderAsc
     * @return Task[]|null
     */
    public static function getTasks($orderBy, $orderType, $pageNum)
    {
        $db = new Db();

        switch ($orderBy)
        {
            case 'userName': $orderBy = Db::ORDER_BY_NAME; break;
            case 'userEmail': $orderBy = Db::ORDER_BY_EMAIL; break;
            case 'status': $orderBy = Db::ORDER_BY_STATUS; break;
        }

        $orderType = strtoupper($orderType) == "ASC";

        $data = $db->select($orderBy, $orderType, $pageNum);

        if (!is_array($data) || empty($data))
            return null;
        foreach ($data as $row)
            $arrTasks[] = self::createTaskByData($row);

        return $arrTasks;
    }


    public function isFinished()
    {
        return $this->status == Db::STATUS_FINISHED;
    }

    public function statusStr()
    {
        return $this->isFinished() ? "Отредактировано админом": "";
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

}