<?php

/**
 * Authorization Class
 */

namespace models;

class Auth
{
    const VALID_LOGIN = "admin";
    const VALID_PASSW = "123";



    public function login($login, $passw)
    {
        $login = strip_tags(addslashes(trim($login)));
        $passw = strip_tags(addslashes(trim($passw)));

        if ($login != self::VALID_LOGIN || $passw != self::VALID_PASSW)
            return false;

        $_SESSION['auth'] = true;
        return true;
    }


    public function getLogin()
    {
        return $this->strLoginAuth;
    }

    private static function createLoginCode($login, $passw)
    {
        return md5($login . "|" . $passw);
    }

    /**
     * Проверка валидности авторизации
     *
     * @param $type = user | tsg
     * @return bool
     */
    public function wbmAuthLogin($type, $login, $passw)
    {

        if (!self::isValidLoginType($type))
            return false;

        self::wbmClear();

        $login = strip_tags(addslashes(trim($login)));
        $passw = strip_tags(addslashes(trim($passw)));

        $this->type = $type;

        $this->strLoginAuth = $login; // strLoginAuth - reserved word
        $this->strPasswAuth = $passw; // strPasswordAuth - reserved word

        $arrAuthInfo = $this->wbmLoadUser(0, $login, $passw);
        // если всё ОК, выставим нужные в дальнейшем переменные
        $isLoginSuccess = false;
        if (is_array($arrAuthInfo) && $this->ID > 0) {
            SessionAuth::setStatusSignIn();
            self::setCookieLoginCode($login, $passw);

            SessionAuth::setAuthInfo($arrAuthInfo);
            $this->setAuthVisitTime();
            $isLoginSuccess = true;

        }

        return $isLoginSuccess;
    }

    private static function getCookieLoginCode()
    {
        if ( (!empty($_COOKIE["signInStatus"]) && strlen($_COOKIE["signInStatus"]) == 32))
            return $_COOKIE["signInStatus"];
        return false;
    }

    private static function setCookieLoginCode($login, $passw)
    {
        $code = self::getCookieLoginCode();
        if ($code == false) {
            $code = SessionAuth::getLoginCode();
            if ($code == false)
                $code = self::createLoginCode($login, $passw);
        }

        setcookie("signInStatus", $code, strtotime("+24 hours"));
        $_COOKIE['signInStatus'] = $code;
    }

    //переопредляемая функция - вызывается при установке в Куки - см setCookieLoginCode
    protected static function getLoginCode()
    {
        return false;
    }


    protected static function clearAuthSession()
    {
        unset($_SESSION['auth']);
    }


}
