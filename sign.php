<?php

use controllers\SignController;

require_once "loader.php";

$ctrl = new SignController();
if ($ctrl->isAuth()) {
    header("Location: /");
    die();
}

if ($ctrl->isSignMode()) {
    if ($ctrl->sign()) {
        header("Location: /");
        die();
    }
}

$ctrl->show();







