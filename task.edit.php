<?php

use controllers\FormController;

require_once "loader.php";

$ctrl = new FormController();

if($ctrl->isEditTaskRequest())
    $ctrl->processRequest();

$ctrl->show();



