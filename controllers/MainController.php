<?php


namespace controllers;


use models\Db;
use models\Task;

class MainController extends BaseController
{
    public $title = "Задачник|Задачи";
    public $blockTitle = "Список задач";

    public function renderContent()
    {
        $page = 0;
        $orderBy = 'userName';
        $orderType = 'asc';

        if (isset($_REQUEST['page']))
            $page = intval($_REQUEST['page']);

        if (isset($_REQUEST['sortField']))
            $orderBy = $_REQUEST['sortField'];
        if (isset($_REQUEST['sortType']))
            $orderType = $_REQUEST['sortType'];

        $arrTasks = Task::getTasks($orderBy, $orderType, $page);
        if (!$arrTasks)
            return "";

        $sortParams = [
            'userName' => ['sortType' => "", 'class' => ""],
            'userEmail' => ['sortType' => "", 'class' => ""],
            'status' => ['sortType' => "", 'class' => ""]
        ];
        $sortParams[$orderBy]['sortType'] = $orderType;
        $sortParams[$orderBy]['class'] = "glyphicon-sort-by-alphabet" . ($orderType == "desc" ? "-alt": "");

        $this->render("table", [
            'arrTasks' => $arrTasks,
            'countPages' => $this->getCountPages(),
            'sort' => $sortParams,
            'pageNum' => $page,
            'sortField' => $orderBy,
            'sortType' => $orderType
        ]);
    }

    public function getCountPages()
    {
        $count = Task::getCount();
        $mod = $count % Db::COUNT_ROWS_TO_FETCH;
        return intval(Task::getCount() / Db::COUNT_ROWS_TO_FETCH) + 1 + ($mod == 0 ? -1: 0);
    }


}