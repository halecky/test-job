<?php


namespace controllers;


use models\Task;

class FormController extends BaseController
{
    const MODE_EDIT = "edit";
    const MODE_CREATE = "create";


    private $_mode = null;
    private $_isErrorProcess = false;
    private $_tplParams = [
        'userName' => "",
        'userEmail' => "",
        'text' => "",
        'checked' => "",
        'btnTitle' => "Создать"
    ];

    public function __construct()
    {
        parent::__construct();
        if (isset($_REQUEST['mode']))
            $this->_mode = $_REQUEST['mode'];
    }

    public function renderContent()
    {
        $params = $this->_tplParams;
        if ($this->isModeEdit()) {
            $taskID = intval($_REQUEST['task_id']);
            $task = Task::createTaskById($taskID);
            $params = [
                'userName' => $task->userName,
                'userEmail' => $task->userEmail,
                'text' => $task->text,
                'checked' => $task->isFinished() ? "checked": "",
                'btnTitle' => "Применить"
            ];
        }
        $this->render("form.create", $params);
    }

    public function isEditTaskRequest()
    {
        return isset($_REQUEST['text']);
    }

    public function processRequest()
    {
        switch ($this->_mode)
        {
            case self::MODE_CREATE:
                $res = $this->processCreate();
                break;

            case self::MODE_EDIT:
                $res = $this->processEdit();
                break;
        }
        $this->_isErrorProcess = $res == false;

        if (!$this->_isErrorProcess)
            $this->gotoMainPage();
    }

    public function isError()
    {
        return $this->_isErrorProcess;
    }

    public function isModeEdit()
    {
        return $this->_mode == self::MODE_EDIT;
    }


    private function processEdit()
    {
        //var_dump($_REQUEST);die();
        $taskID = $_REQUEST['task_id'];
        $data['text'] = $_REQUEST['text'];
        if (!isset($_REQUEST['status']))
            $data['status'] = 0;
        else
            $data['status'] = $_REQUEST['status'] == 'on' ? 1: 0;

        return $this->db()->updateTask($taskID, $data);
    }

    private function processCreate()
    {
        //var_dump($_REQUEST);die();
        $name = $_REQUEST['userName'];
        $email = $_REQUEST['userEmail'];
        $text = $_REQUEST['text'];
        return $this->db()->insertTask($name, $email, $text);
    }

}