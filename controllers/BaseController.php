<?php


namespace controllers;


use models\Db;

class BaseController
{
    const TEMPLATES_PATH = __DIR__ . "/../templates/";
    const TEMPLATE_DEFAULT = "main";

    protected $_templateName = self::TEMPLATE_DEFAULT;
    /** @var Db null  */
    protected $_db = null;

    public $title = "title";
    public $blockTitle = "";

    public function __construct()
    {

    }

    public function render($templateName, $params = [])
    {
        if (!empty($params))
            extract($params);

        return include_once(self::TEMPLATES_PATH . "$templateName.php");

    }

    public function renderContent()
    {

    }

    public function show()
    {
        $this->render($this->_templateName);
    }

    public function isAuth()
    {
        return isset($_SESSION['auth']);
    }

    protected function db()
    {
        if (!$this->_db)
            $this->_db = new Db();
        return $this->_db;
    }

    public function gotoMainPage()
    {
        header("Location: /");
        die();
    }





}