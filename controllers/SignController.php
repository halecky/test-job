<?php


namespace controllers;


class SignController extends BaseController
{
    const VALID_LOGIN = "admin";
    const VALID_PASSW = "123";



    public function renderContent()
    {

        $this->render("sign");
    }

    public function isSignMode()
    {
        return isset($_REQUEST['login']) && isset($_REQUEST['password']);
    }

    public function sign()
    {
        $login = strip_tags(addslashes(trim($_REQUEST['login'])));
        $passw = strip_tags(addslashes(trim($_REQUEST['password'])));

        if ($login != self::VALID_LOGIN || $passw != self::VALID_PASSW)
            return false;

        $_SESSION['auth']['user'] = $login;
        return true;
    }

}