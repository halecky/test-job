$(function () {
    $("body").on('click', "#logout",function(e){
        if (!confirm("Вы уверены, что хотите выйти?"))
            e.preventDefault();
    });

    $("body").on('click', "[sortField]", function () {

        sortField = $(this).attr('sortField');
        sortType = sortType == "desc" ? "asc": "desc";
        pageReloadBySort();

    });

    $("body").on('click', "a[page]",function (e) {
        e.preventDefault();
        currPage = $(this).attr('page');
        pageReloadBySort();



    });



    function pageReloadBySort() {
        var url = "http://" + window.location.host + "/?a=1";
        if (sortField !== "")
            url += "&sortField="+sortField+"&sortType="+sortType;
        if (currPage != undefined)
            url += "&page="+currPage;
        window.location.href = url;
    }



});