<?php

/** @var FormController $this */

use controllers\FormController;

$classType = $this->isModeEdit() ? "disabled": "";


?>

<form role="form" action=""  method="post">
    <h1>Параметры задачи</h1>

    <div class="form-group">
        <label for="userName">Имя пользователя</label>
        <input id="userName" name="userName" value="<?= $userName; ?>" type="text" class="form-control <?= $classType; ?>" placeholder="Имя пользователя" required>

    </div>

    <div class="form-group">
        <label for="userEmail">Email</label>
        <input id="userEmail" name="userEmail" value="<?= $userEmail; ?>" type="email" class="form-control <?= $classType; ?>" placeholder="Еmail" required>
    </div>

    <div class="form-group">
        <label for="text">Задача</label>
        <input id="text" name="text" value="<?= $text; ?>" type="text" class="form-control" placeholder="Текст задачи" required>
    </div>


    <?php if($this->isModeEdit()): ?>
        <div class="checkbox btn-lg">

            <label>
                <input name="status" type="checkbox" <?= $checked; ?> > Задача выполнена
            </label>
        </div>
    <?php endif; ?>

    <?php if($this->isError()): ?>
        <div class="alert alert-danger fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4>Ошибка сервера!</h4>
        </div>

    <?php endif; ?>
    <hr>
    <div>
        <button class="btn btn-lg btn-primary btn-block" type="submit"><?= $btnTitle ?></button>
    </div>





</form>
