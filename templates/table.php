<?php

use controllers\BaseController;
use models\Task;

/** @var Task[] $arrTasks */
/** @var BaseController $this */

?>

<a class="btn btn-success" href="/task.edit.php?mode=create">Создать</a>
<table class="table">
    <tbody>
    <tr>
        <th>
            <button sortField="userName" type="button" class="btn btn-default btn-lg"><span class="glyphicon <?= $sort['userName']['class']; ?>"></span> Имя</button>
        </th>
        <th>
            <button sortField="userEmail" type="button" class="btn btn-default btn-lg"><span class="glyphicon <?= $sort['userEmail']['class']; ?>"></span> Email</button>
        </th>
        <th>
            <button sortField="status"  type="button" class="btn btn-default btn-lg"><span class="glyphicon <?= $sort['status']['class']; ?>"></span> Статус</button>
        </th>
        <th>Задача</th>
        <?php foreach ($arrTasks as $task):
            $rowClass = $task->isFinished() ? "success": "";
        ?>
    <tr task_id="<?= $task->getId(); ?>" class="<?= $rowClass; ?>">
        <td><?= $task->userName; ?></td>
        <td><?= $task->userEmail; ?></td>
        <td>
            <?= $task->statusStr(); ?>
        </td>
        <td><?= $task->text; ?></td>
    </tr>
        <?php endforeach; ?>
    </tr>
    </tbody>
</table>

    <script>
        var sortField = '<?= $sortField; ?>';
        var sortType = '<?= $sortType; ?>';
        var currPage = <?= $pageNum; ?>;
    </script>


<?php if ($countPages > 1): ?>

<nav aria-label="Page navigation">
    <ul class="pagination">


        <?php for($i = 0; $i < $countPages; $i++): ?>
            <li class="page-item"><a page="<?= $i ?>" class="page-link" href="#"><?= $i+1 ?></a></li>
        <?php endfor; ?>



        </li>
    </ul>
</nav>

<?php endif; ?>