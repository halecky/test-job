<?php

use controllers\SignController;

/** @var SignController $this*/



?>
<form class="form-signin" role="form" action="sign.php" method="post">
    <h2 class="form-signin-heading">Авторизация администратора</h2>
    <input name="login" type="text" class="form-control" placeholder="Login" required="" autofocus="">
    <input name="password" type="password" class="form-control" placeholder="Password" required="">

    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

    <?php if($this->isSignMode() && !$this->isAuth()): ?>
        <div class="alert alert-danger fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4>Ошибка авторизации!</h4>

        </div>

    <?php endif; ?>

</form>
